﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleFx : MonoBehaviour {

	public ParticleSystem Particle;


	// Use this for initialization
	public void play () {
		Particle.Play();
	}
	
	// Update is called once per frame
	public void stop (){
		Particle.Stop();
	}
}
